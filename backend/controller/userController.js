import asyncHandler from '../middleware/asyncHandler.js';
import User from '../models/User.js';
import bcryptjs from 'bcryptjs';
import createToken from '../utility/createToken.js';


// CREATE USER REGISTRION CONTROLLER 
const createUser = asyncHandler(async (req, res) => {
	// DESTRUCTURE THE REQ.BODY
	const {
		email,
		password,
		username
	} = req.body;

	// VALIDATE THE DATA
	if (!email || !password || !username) {
		throw new Error('Please enter all fields');
	}

	// PASSWORD MUST BE AT LEAST 6 CHARACTERS
	if (password.length < 6) {
		res.status(400);
		throw new Error('Password must be at least 6 characters');
	}

	// CHECK IF USER ALREADY EXISTS
	const userExist = await User.findOne({
		email
	});
	if (userExist) {
		// Exit the function here to avoid further execution
		return res.status(400).json({
			message: 'User already exists'
		});
	}


	// HASH THE PASSWORD FOR SECURITY
	const salt = await bcryptjs.genSalt(10);
	const hashedPassword = await bcryptjs.hash(password, salt);

	// CREATE A NEW USER
	const newUser = new User({
		username,
		email,
		password: hashedPassword
	});

	try {
		await newUser.save();
		createToken(res, newUser._id);
		const {
			_id,
			username,
			email,
			password,
			isAdmin
		} = newUser._doc;
		res.status(201).json({
			_id,
			username,
			email,
			password,
			isAdmin
		});
	} catch (error) {
		res.status(400);
		throw new Error('Invalid user data');
	}
});



// USER LOGIN CONTROLLER
const userlogin = asyncHandler(async (req, res) => {
	const {
		email,
		password
	} = req.body;

	const userExiting = await User.findOne({
		email
	});

	// Check if the user exists
	if (!userExiting || !userExiting.password) {
		return res.status(401).json({
			message: 'Invalid email or password'
		});
	}




	
	// If the user exists, compare passwords
	const isPasswordValid = await bcryptjs.compare(password, userExiting.password);

	if (isPasswordValid) {
		createToken(res, userExiting._id);
		res.status(200).json({
			message: 'User logged in successfully',
			user: {
				_id: userExiting._id,
				username: userExiting.username,
				email: userExiting.email,
				isAdmin: userExiting.isAdmin,
			}
		});
	} else {
		res.status(401).json({
			message: 'Invalid email or password'
		});
	}
});




// USER LOGOUT CONTROLLER
const userlogout = asyncHandler(async (req, res) => {
	res.cookie('token', '', {
		expires: new Date(Date.now() + 1 * 1000),
		httpOnly: true,
		secure: process.env.NODE_ENV === 'development',
		sameSite: 'strict',
	});
	res.status(200).json({
		message: 'user logged out successfully',
	});
})


// GET ALL USERS CONTROLLER
const getAllUser = asyncHandler(async (req, res) => {
	const users = await User.find({});
	res.status(200).json({
		message: 'All users fetched successfully',
		users,

	})
})


// GET CURRENT USER PROFILE
const getCurrentUserProfile = asyncHandler(async (req, res) => {
	const user = await User.findById(req.user._id);
	if (user) {
		res.status(200).json({
			message: 'User current profile fetched successfully',
			_id: user._id,
			username: user.username,
			email: user.email,
		})
	} else {
		res.status(404)
		throw new Error('User not found');
	}
})






// UPDATE CURRENT USER PROFILE CONTROLLER
const updateCurrentUserProfile = asyncHandler(async (req, res) => {
	const user = await User.findById(req.user._id);

	if (user) {
		user.username = req.body.username || user.username;
		user.email = req.body.email || user.email;
		user.isAdmin = req.body.isAdmin || user.isAdmin;

		if (req.body.password) {
			const salt = await bcryptjs.genSalt(10);
			user.password = await bcryptjs.hash(req.body.password, salt);
		}

		const updatedUser = await user.save();
		res.status(200).json({
			message: 'User profile updated successfully',
			_id: updatedUser._id,
			username: updatedUser.username,
			email: updatedUser.email,
			isAdmin: updatedUser.isAdmin,

		})

	} else {
		res.status(404)
		throw new Error('User not found');
	}
});




// ===================ADMIN CONTROL =========================== //
// DELETE USER
const deleteUserById = asyncHandler(async (req, res) => {
	// res.send("delete", req.params.id);
  	const user = await User.findById(req.params.id);
    if (user) {
		// make sure admin cannot be remove 
        if(user.isAdmin){
          res.status(400);
          throw new Error('Admin cannot be deleted');
        }

		await User.deleteOne({ _id: req.params.id });
        res.json({
          message: 'User deleted successfully'
        });

    }else{
      res.status(404)
      throw new Error('User not found');
    }
});


// GET USER ID
const getUserId = asyncHandler(async(req,res)=>{
	const user = await User.findById(req.params.id).select('-password'); // select is the way to exclude the specific data 

    if (user) {
        res.status(200).json({
            message: 'User fetched successfully',
            _id: user._id,
            username: user.username,
            email: user.email,
            isAdmin: user.isAdmin,
        })
    } else {
        res.status(404)
        throw new Error('User not found');
    }	

})

// UPDATE THE USER ID 
const updateUserId = asyncHandler(async (req, res) => {
	const user = await User.findById(req.params.id);

	if(user){
		user.username = req.body.username || user.username;
        user.email = req.body.email || user.email;
        user.isAdmin = Boolean(req.body.isAdmin);

		if (req.body.password) {
            const salt = await bcryptjs.genSalt(10);
            user.password = await bcryptjs.hash(req.body.password, salt);
        }
		const updatedUser = await user.save();

		res.status(201).json({
			message:"User update successfully",
			_id:updatedUser._id,
			username:updatedUser.username,
            email:updatedUser.email,
            isAdmin:updatedUser.isAdmin,
		});
		
	}else{
		res.status(404)
        throw new Error('User not found');
	}

})

// ===============END ADMIN CONTROLLER ===================== // 

export{
	createUser,
	userlogin,
	userlogout,
	getAllUser,
	getCurrentUserProfile,
	updateCurrentUserProfile,
 	deleteUserById,
	getUserId,
	updateUserId
};






