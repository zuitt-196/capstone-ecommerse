import jwt from "jsonwebtoken";

const generateToken = async (res, userID) => {
  try {
    const token = jwt.sign({ userID }, process.env.JWT_SECRET_KEY, { expiresIn: "30d" });

    // SET THE TOKEN IN THE COOKIE
    try {
      await new Promise((resolve, reject) => {
        res.cookie("JWT", token, {
          httpOnly: true,
          secure: process.env.NODE_ENV === "development",
          sameSite: 'strict',
          maxAge: 30 * 24 * 60 * 60 * 1000, // 30 days
        }, (err) => {
          if (err) {
            console.error(err); // Log the specific error
            reject(err);
          }
          resolve();
        });
      });
    } catch (cookieError) {
      console.error("Error setting cookie:", cookieError);
      throw new Error('Token generation failed');
    }

    return token;
  } catch (jwtError) {
    console.error("Error generating JWT:", jwtError);
    throw new Error('Token generation failed');
  }
};

export default generateToken;
