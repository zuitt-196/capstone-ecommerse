// packages
import path from 'path';
import express from 'express';
import dotenv from 'dotenv';
import cookieParser from 'cookie-parser';
import userRoutes from './routes/userRoutes.js'
dotenv.config();

// utils
import connectDB from './config/db.js';




const app = express();

// BUILT MIDDLEWARE
app.use(express.json());
app.use(express.urlencoded({ extended: true }));  
app.use(cookieParser()); 


connectDB();

// USER  ROUTES
app.use('/api/users', userRoutes)


const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
