import mongoose  from 'mongoose';

const userSchema = mongoose.Schema({  
    username:{
        type: String,
        required: [true, 'plase enter username'],
    },

    email:{
        type: String,
        required: [true, 'plase enter email'],
        unique: true,
    },

    password: {
        type: String,
        required: [true, 'plase enter password'],
        minlength: 6,
    },

    isAdmin:{
        type: Boolean,
        required: true,
        default: false,
    }
    
    
 },
 {
    timestamps: true,
 }
 )

 const User =  mongoose.model('User', userSchema);

 export default User;