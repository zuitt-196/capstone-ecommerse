import express from 'express';
import {
    createUser,
    userlogin,
    userlogout,
    getAllUser,
    getCurrentUserProfile,
    updateCurrentUserProfile,
    deleteUserById,
    getUserId,
    updateUserId
  } from '../controller/userController.js';
import { authenticate, authorizedAdmin } from '../middleware/autheticationMiddleware.js';

const router = express.Router();


// router.route('/').post(createUser).get(authenticate ,authorizedAdmin, getAllUser)

router.post('/', createUser);
router.get('/', authenticate ,authorizedAdmin, getAllUser);
router.post('/auth',userlogin);
router.post('/logout', userlogout);

router.get('/profile' , authenticate , getCurrentUserProfile);
router.put('/profile', authenticate, updateCurrentUserProfile);

// ADMIN ROUTES 
router.delete('/:id', authenticate, authorizedAdmin, deleteUserById);
router.get('/:id' , authenticate , authorizedAdmin , getUserId);
router.put('/:id' , authenticate , authorizedAdmin , updateUserId);



export default router;




