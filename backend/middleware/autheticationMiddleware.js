import JWT from 'jsonwebtoken';
import User from '../models/User.js'
import asyncHandler from './asyncHandler.js';


// authenticate middleware 
const authenticate = asyncHandler(async (req, res, next) => {
    let token;

    token = req.cookies.JWT;
    if(token){
        // console.log(token);
        try {
            const decode = JWT.verify(token, process.env.JWT_SECRET_KEY);
            // console.log(decode);
            req.user = await User.findById(decode.userID).select('-password');
            // console.log(req.user.username);
            next();         

        } catch (error) {
            res.status(401);
            throw new Error('Not authorized, token failed');

        }
    }else{
        res.status(401)
        throw new Error('Not authorized, no token');
    }

})




// Authorized Admin middleware 
const authorizedAdmin = asyncHandler(async (req, res, next) => {
    if (req.user && req.user.isAdmin){
        next()
    }else{
        res.status(401).send('Not authorized, admin only')

    }
})


export {
    authenticate,
    authorizedAdmin
}