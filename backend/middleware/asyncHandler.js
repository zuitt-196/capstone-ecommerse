const asyncHandler = (cb) => (req, res, next) => {
    Promise.resolve(cb(req, res, next)).catch(error =>{
        res.status(500).json({
            message: error.message,
        })
    });
}

export default asyncHandler;



