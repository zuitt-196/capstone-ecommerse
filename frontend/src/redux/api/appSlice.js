import {fetchBaseQuery , craeteApi} from '@reduxjs/toolkit/query/react';
import {BASE_URL} from  './constants';


// DEFINE THE QUERRY BASE IN URL 
const baseQuery =  fetchBaseQuery({baseQuery: BASE_URL});



//  API INDEX  SLICE 
export const apiSlice = craeteApi({
    baseQuery,
    tagTypes:['Product', 'Order', 'User' , "Category"],
    endpoints: () =>({

    })

})