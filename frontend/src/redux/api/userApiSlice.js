import {apiSlice } from './appSlice';
import {USER_URL} from '../constant';




// API USER SLICE ENDPOINTS
export const userAPSlice = apiSlice.injectEndpoints({

    endpoints:(builder) = ({
        login:builder.mutation({
            query:(data) =>({
                url: `${USER_URL}/auth`,
                method: "POST",
                body:data,
            })
        })
    })
})


export const {userLoginMutation } =  userAPSlice;


