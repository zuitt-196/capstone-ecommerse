import React, {useState} from 'react';
import { FaHome , FaShoppingBag , FaCartArrowDown} from "react-icons/fa";
import { MdFavorite } from "react-icons/md";

import { IoIosLogIn } from "react-icons/io";
import { TiUserAdd } from "react-icons/ti";

import { Link ,useNavigate } from 'react-router-dom';

import './Navigation.css'
// import { useState } from "react";



const Navigation = () => {
  const [dropdoneOpen, setDropdoneOpen] = useState(false);
  const [showSidebar, setShowSidebar] = useState(false);


  // DEFINE THE TOOGLE DROPDOWN
  const toogleDropdown = () => {
    setDropdoneOpen(!dropdoneOpen)


  }


// DEFINE THE SLIBAR
  const toogleSidebar = () => {
    setShowSidebar(!showSidebar)
}

// DEFINE THE CLOSEBAR
const closeSidbar = () => {
  setShowSidebar(false)
}



  return (
    <div style={{ zIndex:999 }} className={` ${showSidebar} ? "hidden" : "flex"}  xl:flex lg:flex md:hidden sm:hidden flex-col justify-between p-4 text-white bg-black w-[4%] hover:w-[15%] h-[100vh]  fixed`} id='navigation-container'>

          {/* HOME */}
        <div className='flex flex-col justify-center space-y-4'>
            <Link to='/' className='flex items-center transition-transform transform hover:translate-x-2'>
              <FaHome className='mr-2 mt-[3rem] ' size={40}/>
                <span className='hidden nav-item-name mt-[3rem]'>
                        HOME
                </span>{ ""}
            </Link>


              {/* SHOP */}
            <Link to='/shop' className='flex items-center transition-transform transform hover:translate-x-2'>
              <FaShoppingBag className='mr-2 mt-[3rem] ' size={40}/>
                <span className='hidden nav-item-name mt-[3rem]'>
                        SHOP
                </span>
            </Link>
            

              {/* CART */}
            <Link to='/cart' className='flex items-center transition-transform transform hover:translate-x-2'>
              <FaCartArrowDown className='mr-2 mt-[3rem] ' size={40}/>
                <span className='hidden nav-item-name mt-[3rem]'>
                        CART
                </span>
            </Link>
            

            {/*FAVORITE  */}
            <Link to='/favorite' className='flex items-center transition-transform transform hover:translate-x-2'>
              <MdFavorite className='mr-2 mt-[3rem] ' size={40}/>
                <span className='hidden nav-item-name mt-[3rem]'>
                        FAVORITE
                </span>
            </Link>
            


      

      {/* LOGIN FORM */}
      <ul>
        <li>
        <Link to='/login' className='flex items-center transition-transform transform hover:translate-x-2'>
              <IoIosLogIn className='mr-2 mt-[3rem] ' size={40}/>
                <span className='hidden nav-item-name mt-[3rem]'>
                        login 
                </span>
            </Link>
        </li>
      </ul>
      
      <Link to='/TiUserAdd ' className='flex items-center transition-transform transform hover:translate-x-2'>
              <TiUserAdd className='mr-2 mt-[3rem] ' size={40}/>
                <span className='hidden nav-item-name mt-[3rem]'>
                        User
                </span>
            </Link>

      </div> 
    </div>



  )
}

export default Navigation